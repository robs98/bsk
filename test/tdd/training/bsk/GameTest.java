package tdd.training.bsk;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

public class GameTest {
	
			//Test for User Stories 3 
			@Test
			public void testAddFrame() throws Exception{
				Game ga = new Game();
				Frame fra = new Frame(0,0);
				ga.addFrame(fra);
				ga.addFrame(new Frame(5,4));
				ga.addFrame(new Frame(3,2));
				ga.addFrame(new Frame(3,4));
				ga.addFrame(new Frame(5,2));
				ga.addFrame(new Frame(2,7));
				ga.addFrame(new Frame(1,5));
				ga.addFrame(new Frame(6,3));
				ga.addFrame(new Frame(2,8));
				
				assertEquals(fra,ga.getFrameAt(0));
			}
			
			
			//Test for User Stories 4 
			@Test
			public void testCalculateScore() throws Exception{
				Game ga = new Game();
				ga.addFrame(new Frame(1,5));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(7,2));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(4,4));
				ga.addFrame(new Frame(5,3));
				ga.addFrame(new Frame(3,3));
				ga.addFrame(new Frame(4,5));
				ga.addFrame(new Frame(8,1));
				ga.addFrame(new Frame(2,6));
				
				int score = 81;
				
				
				assertEquals(score,ga.calculateScore());
				
			}
			
			@Test
			//Test for User Stories 5 
			public void testGetScoreWithSpare() throws Exception{
				Game ga = new Game();
				ga.addFrame(new Frame(1,9));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(7,2));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(4,4));
				ga.addFrame(new Frame(5,3));
				ga.addFrame(new Frame(3,3));
				ga.addFrame(new Frame(4,5));
				ga.addFrame(new Frame(8,1));
				ga.addFrame(new Frame(2,6));
				
				int score = 88;
				
				assertEquals(score,ga.calculateScore());
			}
			//Test for User Stories 6 
			@Test
			public void testCalculateScoreWithStrike() throws Exception{
				Game ga = new Game();
				ga.addFrame(new Frame(10,0));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(7,2));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(4,4));
				ga.addFrame(new Frame(5,3));
				ga.addFrame(new Frame(3,3));
				ga.addFrame(new Frame(4,5));
				ga.addFrame(new Frame(8,1));
				ga.addFrame(new Frame(2,6));
				int score = 94;
				
				
				assertEquals(score,ga.calculateScore());
				
			}
			//User stories 7
			@Test
			public void testCalculateScoreWithStrikeAndSpare() throws Exception{
				Game ga = new Game();
				ga.addFrame(new Frame(10,0));
				ga.addFrame(new Frame(4,6));
				ga.addFrame(new Frame(7,2));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(4,4));
				ga.addFrame(new Frame(5,3));
				ga.addFrame(new Frame(3,3));
				ga.addFrame(new Frame(4,5));
				ga.addFrame(new Frame(8,1));
				ga.addFrame(new Frame(2,6));
				int score = 103;
				
				
				
				assertEquals(score,ga.calculateScore());
				
			}
			//User stories 8
			@Test
			public void testCalculateScoreWithMultipleStrike() throws Exception{
				Game ga = new Game();
				ga.addFrame(new Frame(10,0));
				ga.addFrame(new Frame(10,0));
				ga.addFrame(new Frame(7,2));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(4,4));
				ga.addFrame(new Frame(5,3));
				ga.addFrame(new Frame(3,3));
				ga.addFrame(new Frame(4,5));
				ga.addFrame(new Frame(8,1));
				ga.addFrame(new Frame(2,6));
				
				
				assertEquals(112,ga.calculateScore());
				
			}
			
			//User stories 9
			@Test
			public void testCalculateScoreWithMultipleSpare() throws Exception{
				Game ga = new Game();
				ga.addFrame(new Frame(8,2));
				ga.addFrame(new Frame(5,5));
				ga.addFrame(new Frame(7,2));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(4,4));
				ga.addFrame(new Frame(5,3));
				ga.addFrame(new Frame(3,3));
				ga.addFrame(new Frame(4,5));
				ga.addFrame(new Frame(8,1));
				ga.addFrame(new Frame(2,6));
				
				
				assertEquals(98,ga.calculateScore());
				
			}
			
			//User stories 10
			@Test
			public void testCalculateScoreWithSpareAtLastFrame() throws Exception{
				Game ga = new Game();
				ga.addFrame(new Frame(1,5));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(7,2));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(4,4));
				ga.addFrame(new Frame(5,3));
				ga.addFrame(new Frame(3,3));
				ga.addFrame(new Frame(4,5));
				ga.addFrame(new Frame(8,1));
				ga.addFrame(new Frame(2,8));
				ga.setFirstBonusThrow(7);;
				
				
				
				assertEquals(90,ga.calculateScore());
				
			}
			
			//User stories 11
			@Test
			public void testCalculateScoreWithStrikeAtLast() throws Exception{
				Game ga = new Game();
				ga.addFrame(new Frame(1,5));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(7,2));
				ga.addFrame(new Frame(3,6));
				ga.addFrame(new Frame(4,4));
				ga.addFrame(new Frame(5,3));
				ga.addFrame(new Frame(3,3));
				ga.addFrame(new Frame(4,5));
				ga.addFrame(new Frame(8,1));
				ga.addFrame(new Frame(10,0));
				ga.setFirstBonusThrow(7);
				ga.setSecondBonusThrow(2);
				
				
				assertEquals(92,ga.calculateScore());
				
			}
			
			@Test
			// Test for user story 12
			public void testCalculateScoreWithBestScore() throws BowlingException {
				Game game = new Game();
				game.addFrame(new Frame(10,0));
				game.addFrame(new Frame(10,0));
				game.addFrame(new Frame(10,0));
				game.addFrame(new Frame(10,0));
				game.addFrame(new Frame(10,0));
				game.addFrame(new Frame(10,0));
				game.addFrame(new Frame(10,0));
				game.addFrame(new Frame(10,0));
				game.addFrame(new Frame(10,0));
				game.addFrame(new Frame(10,0));
				
				game.setFirstBonusThrow(10);
				game.setSecondBonusThrow(10);
				
					
				
				int score = 300;
				
				assertEquals(score, game.calculateScore());
			}
			
			
			
			
}