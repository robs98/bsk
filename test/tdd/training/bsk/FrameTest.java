package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	//Test for User Stories 1 firstThrow
	public void testGetFirstThrow() throws Exception{
		int first = 4;
		Frame fra = new Frame(first,4);
		assertEquals(first,fra.getFirstThrow());
	}
	@Test
	//Test for User Stories 1 secondThrow
		public void testGetSecondThrow() throws Exception{
			int second = 6;
			Frame fra = new Frame(4,second);
			assertEquals(second,fra.getSecondThrow());
		}
	@Test
	//Test for User Stories 2 
	public void testGetScore() throws Exception{
		int score = 10;
		Frame fra = new Frame(5,5);
		assertEquals(score,fra.getScore());
	}
	

}
